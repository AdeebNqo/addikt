#include<boost/program_options.hpp>
#include<boost/filesystem.hpp>
#include<iostream>
#include<string>
#include<algorithm>
#include<fstream>
bool is_video(boost::filesystem::path &filename, std::vector<std::string> extentions);
int main(int argc, char** argv){
	/*
	Reading video file extentions from file
	*/
	std::vector<std::string> extens;
	std::ifstream vid("vid_extentions.in");
	std::istream_iterator<std::string> curr_ext(vid);
	std::istream_iterator<std::string> end;
	std::move(curr_ext, end, back_inserter(extens));
	/*

	sorting out the program options	

	*/
	namespace po = boost::program_options;
	namespace fs = boost::filesystem;
	po::options_description desc("Allowed options");
	desc.add_options()
    	("help,h", "produce help message")
	("player,p",po::value<std::string>(),"video player")
	("directory,d",po::value<std::string>(),"directory containing video files");

	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);    

	if (vm.count("help")) {
	    std::cout << "Application for stupidly fighting addiction to series'.\nhttps://github.com/AdeebNqo/addikt for more info."<< "\nOptions:\n\t--help,-h Obtain help\n\t--player,-p Video player\n\t--directory,-d Directory containing video files\nAdeeb Nqo<adeebnqo@gmail.com>\n";
	    return 1;
	}

	std::string player;
	if (vm.count("player")){
		player = vm["player"].as<std::string>();
	}
	std::string directory = ".";
	if (vm.count("directory")){
		directory = vm["directory"].as<std::string>();
	}
	//after getting player and directory
	if (player!=""){
		std::cout << player << std::endl;
	}
	if (directory!=""){
		std::cout << directory << std::endl;
	}
	/*
	
	Getting all files in the directory that are video files
	
	*/
	using namespace fs;
	path dir(directory);
	std::vector<path> files;
	std::copy(directory_iterator(dir), directory_iterator(), back_inserter(files));
	return 0;
}
bool is_video(boost::filesystem::path &filename, std::vector<std::string> extentions){
	std::cout << filename.string() << std::endl;
	return true;
}
