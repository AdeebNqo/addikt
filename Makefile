options = -std=c++11 -L/usr/include/ -lboost_program_options -lboost_filesystem -lboost_system
app_name = addikt
##compiler
c = g++

$(app_name): main.cpp
	@$(c) main.cpp -o $(app_name) $(options)
clean:
	@rm $(app_name)
run:
	@./$(app_name)
